Jarke van Wijk's icosahedral foldout from Fig 8 in [1].

Angle of rotation *manually* guessed.

Using "hack" as suggested by [Fil's comment](https://github.com/d3/d3-geo/pull/108#issuecomment-323553563).

[1] Jarke J. van Wijk, "[Unfolding the Earth: Myriahedral Projections](http://www.win.tue.nl/~vanwijk/myriahedral/)", The Cartographic Journal Vol. 45 No. 1 pp. 32–42 February 2008